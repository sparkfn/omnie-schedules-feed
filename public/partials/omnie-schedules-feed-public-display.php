<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       www.onediver.com
 * @since      1.0.0
 *
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/public/partials
 */

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div id="omnie-container" class="omnie-container">
					<!-- <div class="omnie-flex">
						<div class="omnie-search-utils">
							<a id="omnie-btn-filter" class="omnie-btn omnie-btn-default">Search</a>
						</div>
					</div> -->

					<div id="omnie-filter-container" class="omnie-flex">
						<div class="omnie-box">
							<form id="omnie-search-form">
								<div class="omnie-filter-column">
									<div class="omnie-field">
										<label><strong>Name</strong></label>
										<input id="omnie-filter-name" class="omnie-input-text" type="text" name="name" placeholder="Name">    
									</div>
								</div>

								<div class="omnie-filter-column">
									<div class="omnie-field">
										<label><strong>Destination</strong></label>
										<!-- <input id="omnie-filter-destination" class="omnie-input-text" type="text" name="destination" placeholder="Destination"> --> 
										<select id="omnie-filter-destination" name="place_id[]" omnie-filter-destination multiple placeholder="Destination">
											<option value=""></option>
										</select>  
									</div>
								</div>

								<div class="omnie-filter-column">
									<div class="omnie-field">
										<label><strong>Date Start</strong></label>
										<input id="omnie-filter-date-start" class="omnie-input-text" type="text" name="date_start" placeholder="Date Start">    
									</div>
								</div>

								<div class="omnie-filter-column">
									<div class="omnie-field">
										<label><strong>Date End</strong></label>
										<input id="omnie-filter-date-end" class="omnie-input-text" type="text" name="date_end" placeholder="Date End">    
									</div>
								</div>


								<div class="omnie-filter-column">
									<div class="omnie-field">
										<label><strong>Date Start</strong></label>
										<input id="omnie-filter-date-start" class="omnie-input-text" type="text" name="date_start" placeholder="Date Start">    
									</div>
								</div>

								<div class="omnie-filter-column">
									<div class="omnie-field">
										<label><strong>Date End</strong></label>
										<input id="omnie-filter-date-end" class="omnie-input-text" type="text" name="date_end" placeholder="Date End">    
									</div>
								</div>

								<div class="omnie-filter-column-full">
									<div class="text-right" style="margin: 10px;">
										<button type="submit" id="omnie-filter-submit" class="omnie-btn omnie-btn-default" style="width: auto;">Submit</button>
									</div>
								</div>
							</form>
						</div>
					</div>

					<div class="omnie-flex inside-loader">
						<table id="omnie-table" cellspacing="5" cellpadding="5">
							<thead>
								<tr>
									<th style="width: 7%;">Start Date</th>
									<th style="width: 7%;">End Date</th>
									<th style="width: 14%;">Trip</th>
									<th style="width: 14%;">Location</th>
									<th style="width: 7%;">Status</th>    
								</tr>
							</thead>
							<tbody id="table-tbody">
								<!-- Insert Data here -->
							</tbody>
						</table>
					</div>
					
					<div class="omnie-flex">
						<ul id="omnie-pagination" class="pagination-sm"></ul>
					</div>
				</div>