<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.onediver.com
 * @since      1.0.0
 *
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/public
 * @author     race34 <raven@sparkfn.com>
 */
class Omnie_Schedules_Feed_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Omnie_Schedules_Feed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Omnie_Schedules_Feed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/omnie-schedules-feed-public.css', array(), $this->version, 'all' );
		// wp_enqueue_style( 'wloadcss', plugin_dir_url( __DIR__ ) . 'jquery_plugins/wload/css/jquery.Wload.css', array(), $this->version, 'all' );
		// wp_enqueue_style( 'selectize', plugin_dir_url( __DIR__ ) . 'jquery_plugins/selectize/css/selectize.default.css', array(), $this->version, 'all' );
		// wp_enqueue_style( 'jquery-ui-datepicker' , 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Omnie_Schedules_Feed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Omnie_Schedules_Feed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// get api url on option
		// $api_url = get_option('omnie_feed_api_url', '');
		// wp_enqueue_script('jQuery_3_3_1', plugin_dir_url( __DIR__ ) . 'jquery_plugins/jquery.js', array(), null, true);
		// wp_enqueue_script( 'wload', plugin_dir_url( __DIR__ ) . 'jquery_plugins/wload/js/jquery.Wload.js', array( 'jQuery_3_3_1' ), $this->version, true );
		// wp_enqueue_script( 'jqpagination', plugin_dir_url( __FILE__ ) . 'js/jquery-pagination.js', array( 'jQuery_3_3_1' ), $this->version, true );
		// wp_enqueue_script( 'selectize', plugin_dir_url( __DIR__ ) . 'jquery_plugins/selectize/js/selectize.min.js', array( 'jQuery_3_3_1' ), $this->version, true  );
		// wp_enqueue_script( 'omniescript', plugin_dir_url( __FILE__ ) . 'js/omnie-schedules-feed-public.js', array( 'jQuery_3_3_1' ), $this->version, true );
		// wp_localize_script( 'omniescript', 'omniepluginpublic' , array( 
		// 		'api_url' => $api_url
		// 	) 
		// );
	}

	/**
	 * Register the shortcodes.
	 *
	 * @since    1.0.0
	 */
	public function omnie_table_shortcode ($atts = "", $component = "") {

		// shortcode attributes
		$atts = shortcode_atts( array(
			'shop_id' => '',
			'count' => '',
			'date_end' => '',
			'date_start' => '',
			'dropoff_point' => '',
			'event_type' => '',
			'name' => '',
			'pickup_point' => '',
			'place_id' => '',
			'filters' => '',
			'activity_id' => '',
			'available_countries' => '',
			'durations' => ''
		), $atts);

		if (empty($atts['shop_id'])) {
			$view = '<center>' 
						.'<span style="border: 1px solid #b92b27; padding: 10px; color: #b92b27;">'
							.'Parameter \'shop_id\' is required on shortcode [omnieschedules]'
						.'<span>'
					.'</center>';

			return $view;
		}

		// enqueue assets only when using
		// this shortcode
		$this->shortcode_enqueue_assets();
		
		// use assets on this short code
		$this->shortcode_use_assets($atts);
		return $this->shortcode_view($atts);
	}

	/**
	 * Table View
	 */
	public function shortcode_view ($atts) {
		// get filters
		$filters = ( !empty($atts['filters']) ? explode(',', $atts['filters']) : '' );

		// client side view
		$view = '<div id="omnie-container" class="omnie-container">';

		if (!empty($filters)) {

			$view   .= '<div id="omnie-filter-container" class="omnie-flex">
							<div class="omnie-box">
								<form id="omnie-search-form">';
			$view   .=  			'<div class="omnie-flex-layout">';
			
			// trip name
			if (in_array('name', $filters)) {
				$view .=	'<div class="omnie-flex-column">
								<label><strong>Name</strong></label>
								<input id="omnie-filter-name" class="omnie-input-text" type="text" name="omnie_filter_name" placeholder="Name">
							</div>';
			}

			// countries + location
			if (in_array('place', $filters)) {
				$view .=	'<div class="omnie-flex-column">
								<label><strong>Country</strong></label> 
								<select id="omnie-filter-country" name="omnie_filter_country" placeholder="Country">
									<option value=""></option>
								</select>  
							</div>';

				$view .=	'<div class="omnie-flex-column">
								<label><strong>Region</strong></label> 
								<select id="omnie-filter-city" name="omnie_filter_city" placeholder="Region">
									<option value=""></option>
								</select>  
							</div>';

				$view .=	'<div class="omnie-flex-column">
								<label><strong>Location</strong></label> 
								<select id="omnie-filter-destination" name="omnie_filter_destination[]" multiple placeholder="Destination">
									<option value=""></option>
								</select>  
							</div>';
			}

			if (count($filters) === 4 || (count($filters) === 3 && !in_array('name', $filters))) {
				$view .= '</div>';
				$view .= '<div class="omnie-flex-layout">';
			}
			
			// date start & date end
			if (in_array('date', $filters)) {
				$view .= 	'<div class="omnie-flex-column">
								<label><strong>Date Start</strong></label>
								<input id="omnie-filter-date-start" class="omnie-input-text" type="text" name="omnie_filter_date_start" placeholder="Date Start">    
							</div>
							
							<div class="omnie-flex-column">
								<label><strong>Date End</strong></label>
								<input id="omnie-filter-date-end" class="omnie-input-text" type="text" name="omnie_filter_date_end" placeholder="Date End"> 
							</div>';
			}

			// date start & date end
			if (in_array('durations', $filters)) {
				$view .= 	'<div class="omnie-flex-column">
								<label><strong>Durations</strong></label> 
								<select id="omnie-filter-durations" name="omnie_filter_durations[]" multiple placeholder="Durations">
									<option value=""></option>';

									$option_text = '';
									$durations = explode(',', $atts['durations']);
									sort($durations);
									
									foreach($durations as  $duration) {
										switch ((int) $duration) {
											case 1:
												$option_text = 'Day to Day';
												break;
											
											case 2:
												$option_text = '2 Day\'s and 1 night';
												break;

											case 3:
												$option_text = '3 Day\'s and 2 night\'s';
												break;

											case 4:
												$option_text = '4 Day\'s and 3 night\'s';
												break;
											
											case 5:
												$option_text = '5 Day\'s and 4 night\'s';
												break;

											case 6:
												$option_text = '6 Day\'s and 5 night\'s';
												break;
											
											case 7:
												$option_text = '7 Day\'s and 6 night\'s';
												break;

											case 8:
												$option_text = '8 Day\'s and 7 night\'s';
												break;
											
											case 9:
												$option_text = '9 Day\'s and 8 night\'s';
												break;

											case 10:
												$option_text = '10 Day\'s and 9 night\'s';
												break;
											
											case -1:
												$option_text = 'More than 10 days';
												break;
										}

										$view .= "<option value=\"$duration\">$option_text</option>";
									}
				
				$view .=		'</select>  
							</div>';
			}
			
			if (count($filters) === 4 || (count($filters) === 3 && !in_array('name', $filters))) {
				$view .= '</div>';
				$view .= '<div class="omnie-flex-layout">';
			}
			
			// button submit
			if ((count($filters) > 2 && in_array('name', $filters) == false) || count($filters) === 1) {
				$view .= 	'<div class="omnie-flex-column">
								<div class="flex-right omnie-flex-column omnie-flex-right">
									<button type="submit" id="omnie-filter-submit" class="omnie-btn omnie-btn-default">Submit</button>
								</div>
							</div>';
			} else {
				$view .= 	'</div>
							<div class="omnie-flex-layout">
								<div class="flex-right omnie-flex-column omnie-flex-right">
									<button type="submit" id="omnie-filter-submit" class="omnie-btn omnie-btn-default">Submit</button>
								</div>';
			}
			

			$view .= 	'</div>';
			$view .= 	'</form>
					</div>
				</div>';
		}

		// table
		$view .=	'<div class="omnie-flex inside-loader">
						<table id="omnie-table" cellspacing="5" cellpadding="5">
							<thead>
								<tr>
									<th style="width: 7%;">Start Date</th>
									<th style="width: 7%;">End Date</th>
									<th style="width: 14%;">Trip</th>
									<th style="width: 14%;">Location</th>
									<th style="width: 7%;">Status</th> 
									<th style="width: 7%;">Book Now</th>    
								</tr>
							</thead>
							<tbody id="table-tbody">
								<!-- Insert Data here -->
							</tbody>
						</table>
					</div>
					
					<div class="omnie-flex">
						<ul id="omnie-pagination" class="pagination-sm"></ul>
					</div>
				</div>';

		return $view;
	}

	/**
	 * Links and scripts
	 */
	public function shortcode_enqueue_assets () {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/omnie-schedules-feed-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'wloadcss', plugin_dir_url( __DIR__ ) . 'jquery_plugins/wload/css/jquery.Wload.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'selectize', plugin_dir_url( __DIR__ ) . 'jquery_plugins/selectize/css/selectize.default.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'jquery-ui-datepicker' , 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');

		$api_url = get_option('omnie_feed_api_url', '');
		wp_enqueue_script('jQuery_3_3_1', plugin_dir_url( __DIR__ ) . 'jquery_plugins/jquery.js', array(), null, true);
		wp_enqueue_script( 'wload', plugin_dir_url( __DIR__ ) . 'jquery_plugins/wload/js/jquery.Wload.js', array( 'jQuery_3_3_1' ), $this->version, true );
		wp_enqueue_script( 'jqpagination', plugin_dir_url( __FILE__ ) . 'js/jquery-pagination.js', array( 'jQuery_3_3_1' ), $this->version, true );
		wp_enqueue_script( 'selectize', plugin_dir_url( __DIR__ ) . 'jquery_plugins/selectize/js/selectize.min.js', array( 'jQuery_3_3_1' ), $this->version, true  );
		wp_enqueue_script( 'omniescript', plugin_dir_url( __FILE__ ) . 'js/omnie-schedules-feed-public.js', array( 'jQuery_3_3_1' ), $this->version, true );
		wp_localize_script( 'omniescript', 'omniepluginpublic' , array( 
				'api_url' => $api_url
			) 
		);
	}

	/**
	 * Use assets
	 */
	public function shortcode_use_assets ($localize = []) {
		// localize attribs
		wp_localize_script('omniescript', 'omnie_shortcode_attributes', $localize);

		// enqueue css
		wp_enqueue_style($this->plugin_name);
		wp_enqueue_style('jquery-ui-datepicker');
		wp_enqueue_style('wloadcss');
		wp_enqueue_style('selectize');

		// enqueue scripts
		wp_enqueue_script('jQuery_3_3_1');
		wp_enqueue_script('wload');
		wp_enqueue_script('jqpagination');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script('selectize');
		wp_enqueue_script('omniescript');
	}

	/**
	 * Register the shortcodes.
	 *
	 * @since    1.0.0
	 */
	public function register_shortcodes() {
		add_shortcode( 'omnieschedules', array( $this, 'omnie_table_shortcode') );
	}
}
