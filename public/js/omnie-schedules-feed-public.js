// check if jQuery exists
if (typeof jQuery != 'undefined') {  
	// set no conflict to other jquery version
	if (jQuery.fn.jquery == '3.3.1')
		var jQuery_3_3_1 = jQuery.noConflict(true); 
}


(function( $ ) {
	'use strict';
	
	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(function () {
		$(document).ready(function () {

			console.log($(location).attr('hostname') + '/book');

			// fire init
			init();

			$('#omnie-container').show();

			// load on table
			$('body').Wload({
				text:'Loading',
				el: '#omnie-table'
			});

			ajaxRequest(1, '', function (data) {
				$('#table-tbody').html(template(data.events));

				// Get paging
				var paging = data.paging || 0;

				// Initiate pagination
				omniePagination(paging);
			});
			
			
			
			// country location
			$('#omnie-filter-country').change(function () {
				searchDestination($(this).val());
			})

			// country city location
			$('#omnie-filter-city').change(function () {
				searchDestination($('#omnie-filter-country').val(), $(this).val());
			})
			
			// On filter search submit
            $('#omnie-search-form').submit(function (e) {
				e.preventDefault();
				searchFilterFire();
			});
			
			// Action for search
			// button
			searchFilterToggle();
		});

		// initialize
		function init () {
			var omnie_filters = omnie_shortcode_attributes.filters.split(',');

			if (omnie_filters.includes('date')) {
				var dateToday = new Date();
				// filter date start
				jQuery('#omnie-filter-date-start').datepicker({
					minDate: dateToday,
					onSelect: function (date) {
						// filter date end
						$('#omnie-filter-date-end').datepicker("option", "minDate", date);
					}
				}).datepicker('setDate', dateToday)
				
				// filter date end
				jQuery('#omnie-filter-date-end').datepicker({
					minDate: dateToday
				});
			}
			
			if (omnie_filters.includes('place')) {
				fetchCountries();

				// // multiselect with input search
				$('#omnie-filter-country').selectize({
					delimiter: ',',
					maxItems: 1,
				});	

				// // multiselect with input search
				$('#omnie-filter-city').selectize({
					delimiter: ',',
					maxItems: 1,
				});	

				// multiselect with input search
				$('#omnie-filter-destination').selectize({
					delimiter: ',',
					maxItems: 5,
				});
			}
			
			if (omnie_filters.includes('durations')) {
				// // multiselect with input search
				$('#omnie-filter-durations').selectize({
					delimiter: ','
				});	
			}
			
		}

		// pagination
        function omniePagination (paging, filter) {
			// destroy first pagination if it was
			// already initiated.
			$('#omnie-pagination').twbsPagination('destroy');
			paging = paging ? paging : {};

			// Fire pagination, add page items
			if (paging.pageCount > 0) {
				$('#omnie-pagination').twbsPagination({
					totalPages: paging.pageCount,
					visiblePages: 10,
					next: 'Next',
					prev: 'Prev',
					hideOnlyOnePage: true,
					initiateStartPageClick: false,
					onPageClick: function (event, page) {
						
						$('body').Wload({
							text:'Loading',
							el: '#omnie-table'
						})

						//fetch content and render here
						ajaxRequest(page, filter, function (data) {

							if (data.hasOwnProperty('events')) {
								var text = template(data.events);
							}
	
							// appear data
							$('#table-tbody').html(text);
							
							$('html, body').animate({
								scrollTop: $("#table-tbody").offset().top - 80
							}, 2000);

						});
					}
				});	
			}
            
        }

		// table body template
		function template (data) {
			var url = $(location).attr('hostname');
			url = url == 'localhost' ? 'http://localhost/shedules-feed' : 'https://' + url;
			var text;
			if (Array.isArray(data)) {
				data.forEach(function (d) {
					text += "<tr>\n" +
								"<td class=\"omnie-td\">" + DateFormat_(d.date_start) + "</td>\n" + 
								"<td class=\"omnie-td\">" + DateFormat_(d.date_end) + "</td>\n" + 
								"<td class=\"omnie-td\">" + d.name + "</td>\n" + 
								"<td class=\"omnie-td\">" + d.location + "</td>" + 
								"<td class=\"omnie-td\"> Available </td>\n" + 
								"<td class=\"omnie-td omnie-td-book-now\">" + 
									"<center><a target=\"_blank\" href=\"" + url + "/book/" + convertToSlug(d.location[0]) + "?booktype=" + d.name+
									"&start_date=" + YmdDateFormat_(d.date_start, '-') + "&end_date=" + YmdDateFormat_(d.date_end, '-') +
									"\" class=\"omnie-btn omnie-btn-link\">Book Now</a></center>" +
								"</td>\n" + 
							"</tr>\n";
				});
			}

			return text ? text : '<tr><td colspan="6"><center>No records found</center></td></tr>';
		}
		
		// Request schedules data 
		function ajaxRequest (page, filters, callback) {
			var tData = {};

			// set payload
			var payload  = { page: page, sorting: {'Event.date_start': 'desc'} /*, action: 'request_events' */};
			var shop_id = omnie_shortcode_attributes.shop_id;
			var internal_defaults = ['event_type', 'activity_id'];

			if (filters) {
				payload = Object.assign(payload, filters);

				// defaults data on the shortcode
				internal_defaults.forEach(function (st) {
					if (omnie_shortcode_attributes.hasOwnProperty(st)) {
						payload[st] = omnie_shortcode_attributes[st];
					}
				});
				
			} else {
				payload = Object.assign(payload, attributesAsFilter());
			}
			
			// remove empty params
			Object.keys(payload).forEach(function (e) {
				if (!payload[e]) { delete payload[e]; }
			});

			// serialize params
			var params = $.param(payload);
			
			$.get(omniepluginpublic.api_url + shop_id + '/events?' + params)
				.done(function (data) {
					if (data.hasOwnProperty('events')) {
						tData = data;
						$('body').Wload('hide',{ time:3000 });
					}

					if (callback) {
						callback(tData) // some function need to run after the request
					}
				});

			return tData;
		}

		// Formant Date to DD MMM YYYY
		function DateFormat_(date) {
			var newDate = new Date(date);
			// Months short code
			var month_names =["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
			var day = newDate.getDate(); // get day
			var month_index = newDate.getMonth(); // get month
			var year = newDate.getFullYear(); // get year

			return "" + day + " " + month_names[month_index] + " " + year;
		}

		// ymd date format
		function YmdDateFormat_ (date, divider) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
		  
			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
		  
			return [year, month, day].join(divider);
		}

		// Action of search filter button
		function searchFilterToggle () {
            var btnToggle = $('#omnie-btn-filter');
            var filterContainer = $('#omnie-filter-container');

            btnToggle.click(function () {
                filterContainer.toggle();
                var isVisible = filterContainer.is( ":visible" );

                if (isVisible) {
                    btnToggle.html('Close');
					btnToggle.removeClass('omnie-btn-default');
					btnToggle.addClass('omnie-search-close');
                } else {
                    btnToggle.html('Search');
					btnToggle.removeClass('omnie-search-close');
					btnToggle.addClass('omnie-btn-default');
                }
            })
		}
		
		// Fire filter searching, ajax request
		function searchFilterFire () {
            var name = $('#omnie-filter-name').val();
            var destination = $('#omnie-filter-destination').val();
            var date_start = $('#omnie-filter-date-start').val();
			var date_end = $('#omnie-filter-date-end').val();
			var country_code = $('#omnie-filter-country').val();
			var durations = $('#omnie-filter-durations').val();
			
			// change Y-m-d
			date_start = date_start ? YmdDateFormat_(date_start,'-') : '';
			date_end = date_end ? YmdDateFormat_(date_end, '-') : '';
			
            var filter = {
                name: name,
                place_id: destination,
                date_start: date_start,
				date_end: date_end,
				filter: {
					duration: durations
				}
			};
			
			// add loader
			$('body').Wload({
				text:'Loading',
				el: '#omnie-table'
			})

            ajaxRequest(1, filter, function (data) {
				// appear data result
				$('#table-tbody').html(template(data.events));
				
				// re-render pagination
                omniePagination(data.paging, filter);
            });
		}
		
		// Fetch places and append 
		// to select drop down
		function searchDestination (country_code, city) {
			var hasCity = city ? true : false;
			var element = hasCity ? '#omnie-filter-destination' : '#omnie-filter-city';
			

			//$(element)[0].selectize.disable(); // enable select
			disablePlaceSelect(true);

			var payload = {
				count: 1, 
				filter: {
					lk_country_code: country_code
				}
			}

			// if city is defined
			if (city)
				payload = Object.assign(payload, { filter: { city: city }});
			

			fetcher('places', payload, function (data) {
				var count = data.count; // total count of places
				var pages = Math.ceil(count/100);

				// clear selectize options
				var selectize = $(element)[0].selectize;
				selectize.clear();
				selectize.clearOptions();
				selectize.renderCache['option'] = {};
				selectize.renderCache['item'] = {};

				// fetch all pages
				for (var i =  1; i <= pages; i++) {
					payload['count'] = 100;
					payload['page'] = i;
					fetcher('places', payload, function (data) {
						data = $.map( data.places, function(n){ return n; });
						if (hasCity) {
							data = data.map(function (e) {
								return {
									text: e.Place.name,
									value: e.Place.id
								}
							});

							var select = $(element);
						} else {
							data = data.map(function (e) {
								return {
									text: e.Place.city,
									value: e.Place.city
								}
							});
						}

						// flatten deep
						data = $.map( data, function(n){ return n; });
						console.log(element);
						var select = $(element);
						var selectize = select[0].selectize;
						selectize.addOption(data);

						
					});
				}

				// selectize.enable(); // enable select
				disablePlaceSelect(false);
				

			}); // get 1 only to get count
		}

		// disable select
		function disablePlaceSelect (isTrue) {
			var list = ['omnie-filter-country', 'omnie-filter-destination', 'omnie-filter-city'];
			var submit = $('#omnie-filter-submit');

			list.forEach(function (e) {
				if (isTrue) {
					$('#' + e)[0].selectize.disable();
					submit.attr('disabled', true).css('background-color', '#ccc');
				} else {
					$('#' + e)[0].selectize.enable();
					submit.attr('disabled', false).css('background-color', '#fff');
				}
			});
		}

		// get country list and append to
		// country dropdown
		function fetchCountries () {
			fetcher('countries', '', function (data) {
				var available_countries = omnie_shortcode_attributes.available_countries ?  omnie_shortcode_attributes.available_countries.split(',') : [];
				
				data = data.countries.filter(function (e) {
					e = e.code;
					return this.indexOf(e) > -1;
				}, available_countries);

				data = data.map(function (e) {
					return {
						text: e.name,
						value: e.code
					}
				})

				// re-fill/set the selectize values
				var selectize = $("#omnie-filter-country")[0].selectize;

				selectize.clear();
				selectize.clearOptions();
				selectize.renderCache['option'] = {};
				selectize.renderCache['item'] = {};
				selectize.addOption(data);

			});
		}

		// fetch onmie public data
		function fetcher (query_params, params, callback) {
			if (params) params = $.param(params);

			// request data
			$.get(omniepluginpublic.api_url + query_params + '?' + (params ? params : ''))
			.done(function (data) {
				if (callback) {
					callback(data);
				}
			});
		}

		// attributes as filters
		function attributesAsFilter () {
			var atts = omnie_shortcode_attributes;
			var array_list = ['event_type', 'place_id', 'activity_id'];

			// each array element
			array_list.forEach(function (e) {
				if (atts.hasOwnProperty(e)) {
					if (!Array.isArray(atts[e])) {
						atts[e] = atts[e] ? atts[e].split(',').map(function (d) { return d.trim() }) : "";
					}
				}
			})

			return atts;
		}

		// convert string to slug
		function convertToSlug (string) {
			if (string) 
				return string.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
			
			return string;
		}
	});

})( jQuery_3_3_1 );
