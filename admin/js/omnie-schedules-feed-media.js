// check if jQuery exists
if (typeof jQuery != 'undefined') {  
    // set no conflict to other jquery version
    if (jQuery.fn.jquery == '3.3.1')
        var jQuery_3_3_1 = jQuery.noConflict(true); 
}



(function( $ ) {
    'use strict';

    $( window ).on('load',function() {
        
        fetchPlaces();
        AddSelectOptions('countries', '', 'omnie_available_countries');
        AddSelectOptions('event_types', {count: 100}, 'omnie_event_type');
        
        // multiselect with input search
        $('#omnie_place_id').selectize({
            delimiter: ',',
            maxItems: 5
        });

        // Selectize Dropdown
		$('#omnie_event_type').selectize({
			delimiter: ','
        });
        
        // Selectize Dropdown
		$('#omnie_available_countries').selectize({
			delimiter: ','
        });

        // Selectize Dropdown
		$('#omnie_durations').selectize({
			delimiter: ','
        });
        
        // insert short code
        $('.wp-admin').on('click', '.insert-omnie-shortcode-button', function (e) {
            e.preventDefault();
            $('#omnie-validations').removeAttr("style").html('');

			var spinner  = $('.omnie_media_shortcode_spinner');
            var this_button = $('.insert-omnie-shortcode-button');
            spinner.addClass('is-active');
            this_button.prop('disabled', true);

            var scope = '#TB_window ';
            var shop_id = $(scope + '#omnie_shop_id').val();
            var name = $(scope + '#omnie_name').val();
            var date_start = $(scope + '#omnie_date_start').val();
            var date_end = $(scope + '#omnie_date_end').val();
            var event_type = $(scope + '#omnie_event_type').val();
            var pickup_point = $(scope + '#omnie_pickup_point').val();
            var dropoff_point = $(scope + '#omnie_dropoff_point').val();
            var place_id = $(scope + '#omnie_place_id').val();
            var available_countries = $('#omnie_available_countries').val();
            var durations = $('#omnie_durations').val();

            var activity_id = $('input[name^=omnie_activity_id]').map(function(idx, elem) {
                return $(elem).val();
            }).get();

            var filters = $('input[name^=omnie-filters]:checked').map(function(idx, elem) {
                return $(elem).val();
            }).get();
            
            var shortcode = '[omnieschedules';
            var isFalse = false;
            var message = [];

            // not empty shop_id
            if (shop_id) {
                shortcode += ' shop_id="' + shop_id + '"';
            } else {
                isFalse = true;
                message.push('Shop ID is required!');
            }
            
            // not empty name
            if (name) shortcode += ' name="' + name + '"';
            
            // not empty date_start
            if (date_start) shortcode += ' date_start="' + date_start + '"';
            
            // not empty date end
            if (date_end) shortcode += ' date_end="' + date_end + '"';
            
            // not empty event_type
            if (event_type.length > 0) shortcode += ' event_type="' + event_type + '"';
            
            // not empty pickup_point
            if (pickup_point) shortcode += ' pickup_point="' + pickup_point + '"';
           
            // not empty dropoff_point
            if (dropoff_point) shortcode += ' dropoff_point="' + dropoff_point + '"';
            
            // not empty place_id
            if (place_id.length > 0) shortcode += ' place_id="' + place_id + '"';

            // activity_id
            if (activity_id.length > 0) shortcode += ' activity_id="' + activity_id + '"';

            // available_countries
            if (available_countries.length > 0) shortcode += ' available_countries="' + available_countries + '"';

            // available_countries
            if (durations.length > 0) shortcode += ' durations="' + durations + '"';

            // filters
            if (filters.length > 0) shortcode += ' filters="' + filters + '"';
            
            shortcode += ']';

            if (!isFalse) {
                var qParams = {
                    name: name,
                    date_start: date_start,
                    date_end: date_end,
                    event_type: event_type,
                    pickup_point: pickup_point,
                    dropoff_point: dropoff_point,
                    place_id: place_id
                }

                // fetch events check if there is result
                fetchEvents(shop_id, qParams).then(function (isTrue) {

                    // if 
                    if (isTrue) {
                        setTimeout(function(){
                            spinner.removeClass('is-active');
                            this_button.prop('disabled', false);
                            formReset(); // reset form inputs
                            $('#omnie-validations').removeAttr('style').html('')
                            window.send_to_editor(shortcode);   
                        }, 2000);

                        return;
                    }

                    // show error
                    omnie_error_message('No Data Found!', spinner, this_button);
                    
                });
            } else {
                omnie_error_message (message, spinner, this_button);
            }
        });

        /**
         * ===================================================
         *  input field increment
         * ===================================================
         */
        var maxField = 10; //Input fields increment limitation
        var addButton = $('#omnie-add-activity-id-button'); //Add button selector
        var wrapper = $('#omnie-activity-id-table'); //Input field wrapper
        var fieldHTML = '<tr><td><input name="omnie_activity_id[]" type="number" class="omnie-multiple-activity-id" /></td>' +
                        '<td><div class="omnie_remove_button round-button">-</div></td></tr>';

        var omnieActivityIdCounter = 1; //Initial field counter is 1
        
        //Once add button is clicked
        addButton.click(function(){
            //Check maximum number of input fields
            if(omnieActivityIdCounter < maxField){ 
                omnieActivityIdCounter++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });
        
        //Once remove button is clicked
        wrapper.on('click', '.omnie_remove_button', function(e){
            e.preventDefault();

            $(this).parent('td').parent().remove(); //Remove tr on table
            omnieActivityIdCounter--; //Decrement field counter
        });
    })

     // error notification
     function omnie_error_message (message, spinner, button) {
        $('#omnie-validations')
                .css('border', '1px red solid')
                .css('width', '88%')
                .css('padding', '5px')
                .css('overflow-wrap','break-word')  
                .css('color', 'red')
                .html('<strong>' + message + '</strong>');

        $('.media-frame-content').animate({
            scrollTop: $("#omnie-validations").offset().top - 200
        }, 200);

        spinner.removeClass('is-active');
        button.prop('disabled', false);
    }

    // fetch events
    function fetchEvents (shop_id, params) {
        params = $.param(params);

        // fetch event data
        return fetch(omnieplugin.api_url + shop_id + '/events?' + params)
            .then(response => response.json())
            .then(function(data) {
                if (data.hasOwnProperty('paging')) {
                    if (data.paging.count > 0) {
                        return true;
                    } 
                }

                return false;
            })
            .catch(function(e) {
                console.log(e);
                return false;
            });  
    }

    // Fetch places and append 
    // to select drop down
    function fetchPlaces () {
        var select = document.getElementById('omnie_place_id'); // get select field object

        fetcher('places', { "count": 1, "page": 1 }, function (data) {
            var count = data.count; // total count of places
            var pages = Math.ceil(count/100);

            // fetch all pages
            for (var i =  1; i <= pages; i++) {
                fetcher('places', { count: 100, page: i }, function (data) {
                    // flattend deep the array
                    data = $.map( data.places, function(n){ return n; });

                    // append each data
                    $.each(data, function (key, value) {
                        select.options[select.options.length] = new Option(value.Place.name, value.Place.id);
                    });

                });
            }
        }); // get 1 only to get count
    }

    // add options
    function AddSelectOptions (queyry_params, parameters, element_id) {
        var select = document.getElementById(element_id); // get select field object
        var data = fetcher(queyry_params, parameters, function (data) {
            // append each data
            $.each(data[queyry_params], function (key, value) {
                select.options[select.options.length] = new Option(value.name, value.code);
            });
        });
    }

    // fetch onmie public data
    function fetcher (query_params, fetch_params, callback) {
        var tData;

        // serialize params
        if (fetch_params) fetch_params = $.param(fetch_params);

        $.ajaxSetup({async: false});
        $.get(omnieplugin.api_url + query_params + (fetch_params ? '?' + fetch_params : ''))
            .done(function (data) {
                tData = data;
                if (callback) {
                    callback(data);
                }
            });
        
        return tData;
    }

    function formReset () {
        var scope = '#TB_window ';
        $(scope + '#omnie_shop_id').val('');
        $(scope + '#omnie_name').val('');
        $(scope + '#omnie_date_start').val('');
        $(scope + '#omnie_date_end').val('');
        $(scope + '#omnie_event_type').val('');
        $(scope + '#omnie_pickup_point').val('');
        $(scope + '#omnie_dropoff_point').val('');
        $(scope + '#omnie_place_id').val('');
        $(scope + '#omnie_available_countries').val('');
        $(scope + '#omnie_durations').val('');
        $("input[name^=omnie-filters]").each(function(){                
            $(this).prop("checked", true)                
        });
    }
})( jQuery_3_3_1 )