<?php
/**
 * Settings View
 */

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="postbox" id="boxid" style="width: 500px;">
    <div title="Click to toggle" class="handlediv"><br></div>
    <h3 class="hndle">&nbsp;&nbsp;<span>Trip Schedules Settings</span></h3>
    <div class="inside">
        <form action="options.php" method="POST">

            <?php 
                // Settings fields
                settings_fields( 'omnie-schedules-feed-settings' ); 
                do_settings_sections( 'omnie-schedules-feed-settings' );
            ?>
                
            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="my-text-field">Shop ID</label>
                        </th>
                        <td>
                            <input type="number" id="omnie_shop_id" name="omnie_shop_id" style="width: 300px;" value="<?php echo get_option( 'omnie_shop_id' ); ?>">
                            <br>
                            <span class="description">(This will be the default shop id)</span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"></th>
                        <td>
                            <p class="submit"><input type="submit" value="Save Changes" class="button-primary" name="Submit" style="float: right;"></p>
                        </td>
                    </tr>
                </tbody>
            </table>
            
        </form>
    </div>
</div>