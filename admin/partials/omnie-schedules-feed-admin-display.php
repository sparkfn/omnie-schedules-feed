<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       www.onediver.com
 * @since      1.0.0
 *
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/admin/partials
 */

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
