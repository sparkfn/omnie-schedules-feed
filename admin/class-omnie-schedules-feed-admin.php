<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.onediver.com
 * @since      1.0.0
 *
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/admin
 * @author     race34 <raven@sparkfn.com>
 */
class Omnie_Schedules_Feed_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Omnie_Schedules_Feed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Omnie_Schedules_Feed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Omnie_Schedules_Feed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Omnie_Schedules_Feed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

	}

	/**
	 * Add Plugin Menu
	 * 
	 * This will show a menu, Trip Schedules
	 */
	public function add_plugin_menu () {
		add_menu_page('Generate Table For Trip Schedules', 'Trip Schedules', 'manage_options', $this->plugin_name, array($this, 'display_plugin_settings'));
		add_submenu_page( $this->plugin_name, 'Settings', 'Settings', 'manage_options', $this->plugin_name, array($this, 'display_plugin_settings'));
		add_submenu_page( $this->plugin_name, 'Help', 'Help', 'manage_options', $this->plugin_name . '-help', array($this, 'display_plugin_help'));
	}

	/**
	 * Register settings
	 */
	public function register_omnie_settings () {
		register_setting(
			$this->plugin_name . '-settings',
			'omnie_shop_id'
		);
	}

	/**
	 * Plugin Settings view
	 */
	public function display_plugin_settings () {
		include_once 'partials/omnie-schedules-feed-admin-settings.php';
	}

	/**
	 * Plugin Help View
	 */
	public function display_plugin_help () {
		// code here
	}

	/**
	 * Add Settings
	 * 
	 * This will add settings to Trip Schedules
	 */
	public function add_settings_link ($links = []) {
		// this add settings
		$links[] = '<a href="' . admin_url( 'admin.php?page=' . $this->plugin_name ) . '">' . __('Settings') . '</a>';
		return $links;
	}

	/**
	 * Add media button for short code
	 */
	public function omnie_add_media_button () {
		add_thickbox(); // a god damn thickbox
		$this->omnie_media_css();
		$this->omnie_media_scripts();

		echo <<<HTML
<a href="#TB_inline?width=600&height=550&inlineId=add-omnie-shortcode-form" id="add-omnie-shortcode-button" class="button thickbox">Add Omnie Schedules Table</a>
<div id="add-omnie-shortcode-form" style="display:none;">
	<div class="omnie-wizard media-modal wp-core-ui">
		<div class="media-frame">
			<div class="media-frame-title">
				<h1>Omnie Schedules</h1>
				<h2>Shortcode Wizard</h2>
			</div>
			
			<div class="media-frame-content">
				<div style="margin: 5px; padding: 20px;">
					<form id="omnie-media-form">
						<div class="section">
							<div class="section">
								<div id="omnie-validations"></div>
							</div>
							<div class="section">
								<!-- Shop ID -->
								<div class="omnie-layout-flex">
									<div class="omnie-column-flex">
										<label for="omnie_shop_id"><b>Shop ID</b> <small style="color: #0085ba">[shop_id]</small> <br>
											<input id="omnie_shop_id" name="omnie_shop_id" type="number"/>
										</label>
									</div>
								</div>

								<!-- Name -->
								<div class="omnie-layout-flex">
									<div class="omnie-column-flex">
										<label for="omnie_name"><b>Name</b> <small style="color: #0085ba">[name]</small>
											<input id="omnie_name" name="omnie_name" type="text" style="width: 100%; max-width: 100%;" />
										</label>
									</div>
								</div>

								<!-- Date Filters -->
								<div class="omnie-layout-flex">
									<div class="omnie-column-flex">
										<label for="omnie_date_start"><b>Date Start</b> <small style="color: #0085ba">[date_start]</small><br>
											<input id="omnie_date_start" name="omnie_date_start" type="date" style="width: 100%;"/>
										</label>
									</div>
									<div class="omnie-column-flex">
										<label for="omnie_date_end"><b>Date End</b> <small style="color: #0085ba">[date_end]</small><br>
											<input id="omnie_date_end" name="omnie_date_end" type="date" style="width: 100%;"/>
										</label>
									</div>
								</div>

								<!-- Event Types -->
								<div class="omnie-layout-flex">
									<div class="omnie-column-flex">
										<label for="omnie_event_type"><b>Event Types</b> <small style="color: #0085ba">[event_type]</small><br>
											<select id="omnie_event_type" name="omnie_event_type[]" multiple style="width: 100%;"></select>
										</label>
									</div>
								</div>

								<!-- Pickup and Dropoff -->
								<div class="omnie-layout-flex">
									<div class="omnie-column-flex">
										<label for="omnie_pickup_point"><b>Pickup Point</b> <small style="color: #0085ba">[pickup_point]</small>
											<input id="omnie_pickup_point" name="omnie_pickup_point" type="text" style="width: 100%; max-width: 100%;" />
										</label>
									</div>
									<div class="omnie-column-flex">
										<label for="omnie_dropoff_point"><b>Dropoff Point</b> <small style="color: #0085ba">[dropoff_point]</small>
											<input id="omnie_dropoff_point" name="omnie_dropoff_point" type="text" style="width: 100%; max-width: 100%;" />
										</label>
									</div>
								</div>

								<!-- Place ID -->
								<div class="omnie-layout-flex">
									<div class="omnie-column-flex">
										<label for="omnie_place_id"><b>Places</b> <small style="color: #0085ba">[place_id]</small><br>
											<select id="omnie_place_id" name="omnie_place_id[]" multiple style="width: 100%;"></select>
										</label>
									</div>
								</div>

								<!-- Countries -->
								<div class="omnie-layout-flex">
									<div class="omnie-column-flex">
										<label for="omnie_available_countries"><b>Available Countries</b> <small style="color: #0085ba">[available_countries]</small><br>
											<select name="omnie_available_countries[]" id="omnie_available_countries" multiple style="width: 100%;"></select>
										</label>
									</div>
								</div>

								<!-- Durations -->
								<div class="omnie-layout-flex">
									<div class="omnie-column-flex">
										<label for="omnie_durations"><b>Available Durations</b> <small style="color: #0085ba">[durations]</small><br>
											<select name="omnie_durations[]" id="omnie_durations" multiple style="width: 100%;">
												<option value="1">Day to Day</option>
												<option value="2">2 Day's and 1 night</option>
												<option value="3">3 Day's and 2 night's</option>
												<option value="4">4 Day's and 3 night's</option>
												<option value="5">5 Day's and 4 night's</option>
												<option value="6">6 Day's and 5 night's</option>
												<option value="7">7 Day's and 6 night's</option>
												<option value="8">8 Day's and 7 night's</option>
												<option value="9">9 Day's and 8 night's</option>
												<option value="10">10 Day's and 9 night's</option>
												<option value="-1">More than 10 day's</option>
											</select>
										</label>
									</div>
								</div>

								<!-- Activity ID -->
								<div class="omnie-layout-flex" style="margin-top: 10px;">
									<div class="omnie-column omnie-column-flex omnie-flex-border">
										<label><b>Activity ID</b> <small style="color: #0085ba">[activity_id]</small></label>
										<table id="omnie-activity-id-table">
											<tr>
												<td>
													<input name="omnie_activity_id[]" class="omnie-multiple-activity-id" type="number" />
												</td>
												<td>
													<div id="omnie-add-activity-id-button" style="background-color: #5cb85c; color: #fff;" class="round-button">+</div>
												</td>
											</tr>
										</table>
									</div>
									<div class="omnie-column omnie-column-flex omnie-flex-border">
										<label><b>Controll Filters</b> <small style="color: #0085ba">[filters]</small></label>
										<p><input type="checkbox" name="omnie-filters[]" value="name" checked> Name</p>
										<p><input type="checkbox" name="omnie-filters[]" value="place" checked> Place</p>
										<p><input type="checkbox" name="omnie-filters[]" value="date" checked> Date</p>
										<p><input type="checkbox" name="omnie-filters[]" value="durations" checked> Durations</p>
									</div>
								</div>
							</div> <!-- Section End -->
						</div>
					</form>
				</div>	
			</div>
			
			<div class="media-frame-toolbar">
				<div class="media-toolbar">
					<div class="media-toolbar-secondary">
						<a style="float: left;" href="#" onclick="tb_remove(); return false;">Cancel</a>		
					</div>				
					<div class="media-toolbar-primary">
						<div style="text-align: right; float: right; line-height: 23px; margin-top: 5px;">
							<span class="spinner omnie_media_shortcode_spinner" style="float: left;margin: 5px;"></span>
							<input type="button" class="insert-omnie-shortcode-button button button-primary button-large media-button" value="Insert Shortcode">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	

HTML;

	}
	
	/**
	 * Enqueue CSS
	 */
	public function omnie_media_css () {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/omnie-schedules-feed-media-display.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'selectize', plugin_dir_url( __DIR__ ) . 'jquery_plugins/selectize/css/selectize.default.css', array(), $this->version, 'all' );
	}

	/**
	 * Enqueue Scripts
	 */
	public function omnie_media_scripts () {
		$api_url = get_option('omnie_feed_api_url', '');
		wp_enqueue_script('jQuery_3_3_1', plugin_dir_url( __DIR__ ) . 'jquery_plugins/jquery.js', array(), null, true);
		wp_enqueue_script( 'selectize', plugin_dir_url( __DIR__ ) . 'jquery_plugins/selectize/js/selectize.min.js', array( 'jQuery_3_3_1' ), $this->version, true  );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/omnie-schedules-feed-media.js', array( 'jQuery_3_3_1' ), $this->version, true );
		wp_localize_script( $this->plugin_name, 'omnieplugin' , array( 
				'ajax_url' => admin_url( 'admin-ajax.php' ),	
				'api_url' => $api_url
			) 
		);
	}
}
