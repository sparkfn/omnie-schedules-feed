<?php

/**
 * Fired during plugin activation
 *
 * @link       www.onediver.com
 * @since      1.0.0
 *
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/includes
 * @author     race34 <raven@sparkfn.com>
 */
class Omnie_Schedules_Feed_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		$options = array(
			'omnie_feed_api_url' => 'https://omnie-feeder.herokuapp.com/',
			// 'omnie_sf_shop_id' => '',
			// 'omnie_sf_name' => '',
			// 'omnie_sf_date_start' => '',
			// 'omnie_sf_date_end' => '',
			// 'omnie_sf_event_type' => '',
			// 'omnie_sf_pickup_point' => '',
			// 'omnie_sf_dropoff_point' => ''
		);

		// save each empty options or update
		// to empty if exists
		foreach ( $options as $key => $val ) {
			if (get_option($option)) {
				update_option($key, $val, 'no');
			} else {
				add_option($key, $val, '', 'no');
			}
		}
	}

}
