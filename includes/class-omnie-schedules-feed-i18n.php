<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       www.onediver.com
 * @since      1.0.0
 *
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Omnie_Schedules_Feed
 * @subpackage Omnie_Schedules_Feed/includes
 * @author     race34 <raven@sparkfn.com>
 */
class Omnie_Schedules_Feed_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'omnie-schedules-feed',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
