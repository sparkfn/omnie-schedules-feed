<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.onediver.com
 * @since             1.0.0
 * @package           Omnie_Schedules_Feed
 *
 * @wordpress-plugin
 * Plugin Name:       Omnie Schedules feed
 * Plugin URI:        www.onediver.com
 * Description:       A plugin that generate tables of Omnie Schedules List.
 * Version:           1.0.0.1
 * Author:            race34
 * Author URI:        www.onediver.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       omnie-schedules-feed
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0.1' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-omnie-schedules-feed-activator.php
 */
function activate_omnie_schedules_feed() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-omnie-schedules-feed-activator.php';
	Omnie_Schedules_Feed_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-omnie-schedules-feed-deactivator.php
 */
function deactivate_omnie_schedules_feed() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-omnie-schedules-feed-deactivator.php';
	Omnie_Schedules_Feed_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_omnie_schedules_feed' );
register_deactivation_hook( __FILE__, 'deactivate_omnie_schedules_feed' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-omnie-schedules-feed.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_omnie_schedules_feed() {

	$plugin = new Omnie_Schedules_Feed();
	$plugin->run();
	
}

/**
 * Fetch Omnie Event Types
 */
function OmnieEventTypeFetch () {
	
	// Get Api Url from option
	$api_url = get_option('omnie_feed_api_url', '');

	// Fetch Data
	$request = wp_remote_request( $api_url . 'event_types?count=100' );

	// Get Event Types only from response
	$response = wp_remote_retrieve_body($request);

	// set type of response as array
	$data = (array) json_decode($response);
	
	return $data;
}

/**
 * Convert object to array
 */
function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}
	
	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}

run_omnie_schedules_feed();
